#!/bin/bash
#
# description: firewall4
# Enable and build firewall up, based on rules written in /etc/firewall4
#NOTE: check /tmp/firewall-v4.sh if you want to see all executed iptables commands

#RELEASES
#4.2.1: adding the ability to create and load sets in text files, named:
#       listname.POLICY.family
#       POLICY=ACCEPT, REJECT, DROP
#       family= inet(IPv4), inet6(IPv6), domain (can contain AS, domains, IPv4 or IPv6)
#       --> IPv4 and IPv6 sets will be generated from the 3 types.
#
#       most functions are now external, and included at the beginning of this script (see "functions.xxx files")

# shellcheck source=/util/functions.misc
. /util/functions.misc   2>/dev/null || . ~/util/functions.misc   2>/dev/null || . ./functions.misc   2>/dev/null
# shellcheck source=/util/functions.net
. /util/functions.net    2>/dev/null || . ~/util/functions.net    2>/dev/null || . ./functions.net    2>/dev/null
# shellcheck source=/util/functions.string
. /util/functions.string 2>/dev/null || . ~/util/functions.string 2>/dev/null || . ./functions.string 2>/dev/null
# shellcheck source=/util/functions.ipset
. /util/functions.ipset 2>/dev/null || . ~/util/functions.ipset 2>/dev/null || . ./functions.ipset 2>/dev/null

trap ctrl_c INT

amIRoot "You must be root!!!"

VERSION=4.2.2
VERSION_DATE=2023-03-19_01-00
OUTPUT=$(tmpFileName "/tmp/firewall-$VERSION-").sh
AUX=/util

CONFDIR=/etc/firewall4                                  #configuration files: config and flows

header()
{
  echo "#Commands will be saved into $OUTPUT" | tee -a "$OUTPUT"
  echo "#Date/time of execution: $(date)" | tee -a "$OUTPUT"
  echo "#Build: $VERSION $VERSION_DATE" | tee -a "$OUTPUT"
  echo
}

fwLoadSet()
{
  cd $CONFDIR || exit
  for POLICY in ACCEPT DROP REJECT
  do
    for F in inet inet6
    do
      if  [ -r "$SET.$POLICY.$F" ]
      then
        echo "#loading set $SET.$POLICY.$F" | tee -a "$OUTPUT"
        execute "cat ./$SET.$POLICY.$F | sed -e 's/#.*//g' -e 's/[ \t]*//g' | xargs -r -n 1 ipset add $POLICY.$F">>"$OUTPUT" 2>&1
      fi
    done
    for F in domains
    do
      if  [ -r "$SET.$POLICY.$F" ]
      then
        echo "#loading set $SET.$POLICY.$F" | tee -a "$OUTPUT"
        execute "cat ./$SET.$POLICY.$F | sed -e 's/#.*//g' -e 's/[ \t]*//g' | $AUX/dns.name.to.ipv4 | xargs -r -n 1 ipset add $POLICY.inet">>"$OUTPUT" 2>&1
        execute "cat ./$SET.$POLICY.$F | sed -e 's/#.*//g' -e 's/[ \t]*//g' | $AUX/dns.name.to.ipv6 | xargs -r -n 1 ipset add $POLICY.inet6">>"$OUTPUT" 2>&1
      fi
    done
  done
}

if [ $# -eq 0 ]
then
  echo "$0 set..."
  echo
  echo "sets are loaded from files in directory $CONFDIR"
  echo
  echo "the file names must follow the structure \"setname.POLICY.FAMILY\" where:"
  echo "POLICY must be ACCEPT, DROP or REJECT"
  echo "FAMILY must be inet, inet6 or domains"
  echo 
  echo "\"domains\" is a list of domain names: the will be resolved and added to POLICY.inet and POLICY.inet6"
  echo
  echo "sets can be quite large, so they are loaded in background"
  exit 1
fi

for SET in "$@"
do
  family="${SET##*.}"
  echo "#loading sets from $CONFDIR..." | tee -a "$OUTPUT"
  background fwLoadSet
done
