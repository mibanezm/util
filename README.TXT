Interesting techniques used in some scripts:

- functions.* : auxiliary bash functions
- functions.net (numToIPv4Addr): exit values on function parameters
- functions.net: BASH_REMATCH
- functions.date: append to array, array output using nameref (local -n), declare -p array...
- bash.template: standard template for bash scripts
- bash.comments.delete: read from specified files in the command line, or from stdin if no files are explicitly used
- crypt.c: DES implementation in C language
- getopt.example: use of getopt (recommended)
- getopts.example: use of getopts
- combine: "while read" loop
- ip.sort: sorting with numeric keys, reading from stdin in case no parameters are present
