#!/usr/bin/perl -w
############################################
# cat /etc/cron.daily/checkRaid.pl 

use strict;
use POSIX;
use Sys::Hostname;
use File::Basename;


use vars qw( $msg $debug $host );

###############################################################################
# SUB:        sendmail
# PURPOSE:    mail message
#               
# ARGS:        $msg     - the message to be mailed
#
# RETURNS:    n/a
###############################################################################
sub sendmail {
  my $msg = $_[0];

  ### Of course, the sysadmin should have editted /etc/aliases and added a 
  ### real person to the alias for root!  Example:
  ##|  # grep '^root' /etc/aliases
  ##|  raid:           root, admin@abc.com
  ##|  # newaliases
  ##
  my $mailfrom = 'root@localhost';
  my $mailtarget = 'adminservers@iescierva.net';
  my $sm = "/usr/sbin/sendmail -t";
  open( SENDMAIL, "|$sm" ) or die( "Cannot open $sm: $!" );
  print SENDMAIL "From: $mailfrom\n";
  print SENDMAIL "Reply-to: $mailtarget\n";
  print SENDMAIL "To: $mailtarget\n";
  print SENDMAIL "Subject: Raid Status Warning\n";
  print SENDMAIL "Content-type: text/plain\n\n";
  print SENDMAIL "Dear System Adminstrator:\n\n";
  print SENDMAIL "$msg\n";
  close( SENDMAIL );
}

my $usage= "
  USAGE:\t$0 [ -v ]
  WHERE:
   -v\t - turns on verbose debugging\n";

( $#ARGV >= -1 and $#ARGV <= 0 ) or die( $usage );
$debug = 0;
while( (my $arg = shift @ARGV) ) {
  if( $arg eq "-v" ) {
    $debug = 1;
  }
  else {
    die( $usage );
  }
}


my $host = hostname() ;
## Run Raid tool to get Raid status 
my ($cmdopt,$cmd) ;
my $msg_flag = 0 ;
#my $statf = POSIX::tmpnam() ;
my $statf = File::Temp ;
my $raidcmd="/usr/sbin/megacli" ;
 $cmdopt = " -CfgDsply -aALL -nolog |grep 'State               :'" ;
 $cmd= $raidcmd.$cmdopt." >".$statf ;
 my $res = system( $cmd ) ;
 if( $res == 0 ) {
   if( !open( FILE, $statf ) ) {
      $msg = "cmd:".$cmd." on ".$host." got wrong\n" ;
      $msg.=sprintf( "cannot open $statf: $!\n" ) ;
      $msg_flag =1 ;
   }
   my @status = <FILE> ;
   if( !close( FILE ) ) {
      $msg = "cmd:".$cmd." on ".$host."got wrong\n" ;
      $msg.=sprintf( "cannot close $statf: $!\n" ) ;
      $msg_flag =1 ;
   }
   unlink $statf ;
   my( $entry ) ;
   $msg.=sprintf(" Raid Status Report on host %s:\n Tool %s%s\n ",$host,$raidcmd,$cmdopt);
   foreach $entry (@status) {
     chomp( $entry ) ;
     $entry =~ s/^(\s)+// ;
     my @fields = split /[:]/, $entry ;
     if($fields[0] eq "State               " ) {
       if ( $fields[1] ne " Optimal" ) {
         $msg.=sprintf(" Raid Status Report :\n Tool %s\n Raid status on host %s is %s \n",$raidcmd,$host,$fields[1]) ; 
         $msg_flag =1 ;
       }
       elsif($debug) {
         $msg.=sprintf(" Raid Status Report :\n Tool %s\n Raid status on host %s is %s \n",$raidcmd,$host,$fields[1]) ; 
         $msg_flag =1 ;
       }
     }
   }
  }
  else {
      $msg = sprintf( "Raid tool %s returned wrong value %d on host %s .\n",$raidcmd,$res,$host ) ;
      $msg .= sprintf( "Pls check %s on host %s .\n",$raidcmd, $host ) ;
      $msg_flag =1 ;
  }
if($msg_flag) {
   if($debug) {
     print $msg ;
     #sendmail( $msg ) ;
   }
   else {
     sendmail( $msg ) ;
   }
}
else {
  open LOG,">>/var/log/raidcheck.log" or die "Cannot open logfile : $!\n";
  my $ltime = strftime( "%b %d %H:%M:%S", localtime() );
  print LOG "$ltime Raid checks are happy\n";
  close LOG;
}
