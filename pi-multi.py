#!/usr/bin/python


import threading
from mpmath import mp
import time

# Establecer la precisión (número de dígitos de Pi)
mp.dps = 1000  # Ajuste este valor según sea necesario


def chudnovsky_formula(start, end, result, index):
    """
    Calcula una porción del algoritmo de Chudnovsky desde start hasta end.
    El resultado se almacena en result[index].
    """
    pi_part = mp.mpf(0)
    for k in range(start, end):
        numerator = mp.fac(6*k)*(545140134*k + 13591409)
        denominator = mp.fac(3*k)*(mp.fac(k)**3)*(mp.power(640320, 3*k + 1.5))
        pi_part += mp.mpf((-1)**k) * mp.mpf(numerator) / mp.mpf(denominator)
    result[index] = pi_part * 12


def calculate_pi_parallel(n_terms, n_threads):
    """
    Calcula Pi utilizando n_terms del algoritmo de Chudnovsky y n_threads hilos
    """
    threads = []
    result = [mp.mpf(0)] * n_threads
    chunk_size = n_terms // n_threads

    for i in range(n_threads):
        start = i * chunk_size
        end = start + chunk_size if i < n_threads - 1 else n_terms
        thread = threading.Thread(target=chudnovsky_formula, args=(start, end, result, i))
        threads.append(thread)
        thread.start()

    for thread in threads:
        thread.join()

    pi_estimate = sum(result)
    return 1 / pi_estimate

start_time = time.time()
n_terms = 1000  # Ajuste este valor para cambiar la precisión y el rendimiento
n_threads = 4  # Ajuste este valor según el número de cores disponibles
pi_estimate = calculate_pi_parallel(n_terms, n_threads)
print(f"Estimación de Pi: {pi_estimate}")
print(f"Tiempo de ejecución: {time.time() - start_time} segundos")

