#!/usr/bin/python

from decimal import Decimal, getcontext
import math
import time


def calcular_pi_chudnovsky(precision):
    """
    Calcula pi utilizando el algoritmo de Chudnovsky.

    Parámetros:
    - precision: El número de dígitos de pi que se desea calcular.

    Retorna:
    - pi calculado con la precisión especificada.
    """
    # Establecer la precisión para los cálculos decimales
    getcontext().prec = precision + 1

    C = 426880 * Decimal(math.sqrt(10005))
    M = 1
    L = 13591409
    X = 1
    K = 6
    S = L

    for i in range(1, precision):
        M = (K**3 - 16*K) * M // i**3
        L += 545140134
        X *= -262537412640768000
        S += Decimal(M * L) / X
        K += 12

    pi = C / S
    return pi


# Ejemplo de uso: calcular 100 dígitos de pi
precision = 1000
start_time = time.time()  # Registrar el tiempo de inicio
pi_calculado = calcular_pi_chudnovsky(precision)
end_time = time.time()  # Registrar el tiempo de finalización
print(f"Pi con {precision} dígitos:\n{pi_calculado}")
print(f"Tiempo total de ejecución: {end_time - start_time} segundos")
