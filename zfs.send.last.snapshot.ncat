#!/bin/bash

################################################################################
#zfs.send.last.snapshot.ncat
################################################################################
# General Description:
# send a zfs dataset to a remote destination via ssh pipeline
# This command uses "ncat" to avoid compression and speed up the process
# Parameters:
#   source filesystem or volume (snapshots not allowed)
#   -i|--identity: ssh private IDENTITY
#   -h|--host: destination host
#   -d|--destination: destination dataset ("zfs receive" -F option will be used)
#   -r|--recursive: all descendants
#   -R|--replication: replicate (all descendants and snapshots)
################################################################################

exit_abnormal()
{
  printf "\nUsage:\n\t%s src-dataset {-i|--identity} ssh-priv-key {-h|--host} dest-host {-d|--destination} dest-dataset [-r|-R]\n" "$0"
  printf "\t-r: recursive (all descendants)\n"
  printf "\t-R: replication (all descendants and snapsots)\n\n"
  exit 1
}

. /util/functions.misc || ~/util/functions.misc
trap ctrl_c INT

TEMP=$(getopt -o d:i:h:rR --long destination:,identity:,host:,recursive,replication -n 'zfs.send.last.snapshot.ncat' -- "$@")
eval set -- "$TEMP"

FLAGREC=""
while true;
do
  case "$1" in
    -d | --destination ) DST=$2; shift 2 ;;
    -h | --host ) HOST=$2; shift 2 ;;
    -i | --identity ) IDENTITY=$2; shift 2 ;;
    -r | --recursive ) FLAGREC="-r"; OPT=""; shift 1 ;;
    -R | --replication ) FLAGREC="-R"; OPT="-R"; shift 1 ;;
    -- ) shift; break ;;
    *) exit_abnormal ;;
  esac
done

if [ "$#" -ne 1 ] || [ -z "$DST" ] || [ -z "$HOST" ] || [ -z "$IDENTITY" ]
then
  exit_abnormal
fi

#Parameters
LISTENERTIMEOUT=30
SENDERTIMEOUT=30
SLEEP=5
BLKSIZE=64k
BUFSIZE=1M

for P in pv mbuffer nmap
do
  if ! which $P &> /dev/null
  then
    amIRoot "for apt-get"
    apt-get update  &>/dev/null
    apt-get install --yes $P &>/dev/null
  fi
done

amIRoot "for zfs send"
SRC=$1
TYPE=$(zfs get type -o value -H "$SRC")

if [ "$TYPE" == "filesystem" ] || [ "$TYPE" == "volume" ]
then
  #get sizes (in bytes)
  usedbydataset=$(zfs get usedbydataset -o value -Hp "$SRC")
  usedbychildren=$(zfs get usedbychildren -o value -Hp "$SRC")
  usedbysnapshots=$(zfs get usedbysnapshots -o value -Hp "$SRC")

  if [ "$FLAGREC" == "-R" ]
  then
    usedtotal=$((usedbydataset+usedbychildren+usedbysnapshots))
    datasets="$SRC"
  elif [ "$FLAGREC" == "-r" ]
  then
    usedtotal=$((usedbydataset+usedbychildren))
    datasets=$(zfs list -r -H -o name "$SRC")
  else
    usedtotal=$usedbydataset
    datasets="$SRC"
  fi

  dataset1=$(echo $datasets | cut -d' ' -f1)

  echo usedbydataset="$(numfmt --to=iec-i --suffix=B "$usedbydataset")"
  echo usedbychildren="$(numfmt --to=iec-i --suffix=B "$usedbychildren")"
  echo usedbysnapshots="$(numfmt --to=iec-i --suffix=B "$usedbysnapshots")"
  echo usedtotal="$(numfmt --to=iec-i --suffix=B "$usedtotal")"

  #Find the very recent snapshot
  LAST=$(/util/zfs.list.snapshots "$SRC" -i -s | head -1 )
  SNAP=$(echo "$LAST" | cut -f2 -d'@')

  echo Last Snapshot: "$LAST"

  #if exists
  if [ -n "${LAST}" ]
  then
    for DS in $datasets
    do
      if [[ "$FLAGREC" =~ "-R" ]]
      then
        size=$usedtotal
      elif [[ "$FLAGREC" =~ "-r" ]]
      then
        size=$(zfs get usedbydataset -o value -Hp "$DS")
      else
        size=$usedtotal
      fi
      SUBSET=${DS//$dataset1/}
      PORT=$(/util/random.free.tcp.port 1 6730)

      echo receiver: ssh -i ${IDENTITY} ${HOST} "iptables -I INPUT -p tcp --dport ${PORT} -j ACCEPT; ncat --listen --wait ${LISTENERTIMEOUT} ${PORT} | mbuffer -q -s $BLKSIZE -m $BUFSIZE | zfs receive -Fu ${DST}${SUBSET}; iptables -D INPUT -p tcp --dport ${PORT} -j ACCEPT"
      echo sender:   zfs send ${OPT} ${DS}@${SNAP} \| mbuffer -q -s $BLKSIZE -m $BUFSIZE \| pv -peatrb -s $size \| ncat --wait ${SENDERTIMEOUT} ${HOST} $PORT

      echo 'prepare sender...'
      ( sleep $SLEEP; zfs send ${OPT} "${DS}@${SNAP}" | mbuffer -q -s $BLKSIZE -m $BUFSIZE | pv -peatrb -s $size | ncat --wait ${SENDERTIMEOUT} ${HOST} $PORT ) &

      echo prepare ncat receiver on port $PORT...
      ssh -i ${IDENTITY} ${HOST} "iptables -I INPUT -p tcp --dport ${PORT} -j ACCEPT; ncat --listen --wait $LISTENERTIMEOUT ${PORT} | mbuffer -q -s $BLKSIZE -m $BUFSIZE | zfs receive -Fu ${DST}${SUBSET}; iptables -D INPUT -p tcp --dport ${PORT} -j ACCEPT"


    done
  else
    printf "\nNo snapshots, nothing sent!!!\n\n"
    exit 1
  fi
else
  printf "\ndataset is neither a filesystem nor a volume, nothing to do!!!n\n"
fi
