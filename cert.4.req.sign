#!/bin/bash

################################################################################
# cert.4.req.sign
################################################################################
# General Description:
# sign a CSR stored in cert hierarcy with a CA in the same hierarchy
################################################################################

if [ ! $# -eq 2 ]
then
  printf "\n\nUsage:\n\t%s CSR CA\n\n" "$0"
  exit 1
fi

. /util/functions.misc   2>/dev/null || . ~/util/functions.misc   2>/dev/null || . ./functions.misc   2>/dev/null

trap ctrl_c INT

prg=$0
if [ ! -e "$prg" ]; then
  case $prg in
    (*/*) exit 1;;
    (*) prg=$(command -v -- "$prg") || exit;;
  esac
fi
dir=$(
  cd -P -- "$(dirname -- "$prg")" && pwd -P
) || exit
prg=$dir/$(basename -- "$prg") || exit
owndir=$(dirname "$prg")

. "$owndir/cert.0.vars"

BASE="$CERT_BASE/signed"
BASECSR="$CERT_BASE/requests"
BASECA="$CERT_BASE/ca"

C=$1
CA=$2

if [ ! -d "$BASECA/$CA" ]
then
     printf "\n\nNon existent: %s/%s. Create your CA firstly.\n\n" "$BASECA" "$CA"
     exit 1
fi

if [ ! -d "$BASECSR/$C" ]
then
     printf "\n\nNon existent: %s/%s. Create your CSR firstly.\n\n" "$BASECSR" "$C"
     exit 1
fi

if [ -f "$BASE/private/$C.pem" ]
then
     printf "\n\n%s/private/%s.pem exists!!! Won't be overwritten.\n\n" "$BASE" "$C"
     exit 1
fi

if [ -f "$BASE/public/$C.pem" ]
then
     printf "\n\n%s/public/%s.pem exists!!! Won't be overwritten\n\n" "$BASE" "$C"
     exit 1
fi

CNF_CMD="-config $BASECSR/$C/conf/openssl.cnf"
CSR=$BASECSR/$C/public/$C.csr
CERT=$BASE/public/$C.pem
#PKEY=$BASE/private/$C.pem
CACERT=$BASECA/$CA/public/$CA.pem
CAKEY=$BASECA/$CA/private/$CA.pem
KEY=$BASECSR/$C/private/$C.pem
KEYCOPY=$BASE/private/$C.pem
P12=$BASE/public/$C.p12
#CA_CMD="-signkey $CAKEY -CA $CACERT -CAkey $CAKEY -CAcreateserial"

mkdir -p $BASE/{public,private} >/dev/null 2>&1
chmod 700 "$BASE/private"

cd "$BASECSR" || exit

echo eliminando passphrase...
execute "openssl rsa -in $KEY -out $KEY"

cp "$KEY" "$KEYCOPY"

cd "$BASECA/$CA" || exit

echo "Firmando y generando $CERT..."
if execute "openssl ca -policy policy_match -out $CERT $CNF_CMD -extensions v3_req -keyfile $CAKEY -cert $CACERT -infiles $CSR"
then 
  chmod 0700 ${BASE}
  echo Converting to PKCS#12...
  execute "openssl pkcs12 -export -in $CERT -inkey $KEY -out $P12 -name $P12"
fi
