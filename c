#!/bin/bash

################################################################################
#command
################################################################################
# General Description:
# compile, link and strip a single .c file
################################################################################

if [ $# -ne 1 ]
then
  printf "\n\nUsage:\n\t%s\n\n" "$0"
  exit 1
fi

. /util/functions.misc || ~/util/functions.misc
trap ctrl_c INT

cc -o $1 $1.c
strip $1
