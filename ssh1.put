#!/bin/bash

################################################################################
sh1.put
################################################################################
# General Description:
# copy a local file to a remote site or device via scp1 (from ssh1)
# 
# Notes:
# - sshpass and ssh1 packages are required
#
# Parameters:
# - (p) password
# - (u) user
# - (h) host
# - (l) localfile
# - (o) option (multiple) (optional)
# - (d) destination
# - (v) verbose (optional)
# - (a) apt (optional) : apt install required packages
# - (i) identity (optional): use an identity file, instead of a password
#
# Examples:
#
# Return value:
# - 0=OK
# - 1=Error
################################################################################

. /util/functions.misc   2>/dev/null || . ~/util/functions.misc   2>/dev/null || . ./functions.misc   2>/dev/null
#amIRoot "Reason"
trap ctrl_c INT

#!/bin/bash

# Define the options
OPTIONS=p:u:h:l:o:d:vi:a
LONGOPTIONS=password:,user:,host:,localfile:,option:,destination:,verbose,identity:,apt

# Default values for variables
password=""
user=""
host=""
localfile=""
options=()
destination=""
verbose_option=""
identity_file=""
apt_install=0

# Function to install packages
install_packages() {
    echo "Installing sshpass and openssh-client..."
    sudo apt-get update
    sudo DEBIAN_FRONTEND=noninteractive apt-get install -y sshpass openssh-client-ssh1
    # Replace openssh-client with openssh-client-ssh1 or the specific package for SSH protocol 1 if necessary.
}

# Parse the options
PARSED=$(getopt --options=$OPTIONS --longoptions=$LONGOPTIONS --name "$0" -- "$@")
if [ $? != 0 ]; then
    # getopt has complained about wrong arguments to stdout
    exit 2
fi
# use eval with "$PARSED" to properly handle the quoting
eval set -- "$PARSED"

# Extract options and their arguments into variables.
while true; do
    case "$1" in
        -p|--password)
            password="$2"
            shift 2
            ;;
        -u|--user)
            user="$2"
            shift 2
            ;;
        -h|--host)
            host="$2"
            shift 2
            ;;
        -l|--localfile)
            localfile="$2"
            shift 2
            ;;
        -o|--option)
            options+=("$2")
            shift 2
            ;;
        -d|--destination)
            destination="$2"
            shift 2
            ;;
        -v|--verbose)
            verbose_option="-v"
            shift
            ;;
        -i|--identity)
            identity_file="$2"
            shift 2
            ;;
        -a|--apt)
            apt_install=1
            shift
            ;;
        --)
            shift
            break
            ;;
        *)
            echo "Programming error"
            exit 3
            ;;
    esac
done

# Install packages if requested
if [ "$apt_install" -eq 1 ]; then
    install_packages
fi

# Check for mandatory options
if [ -z "$user" ] || [ -z "$host" ] || [ -z "$localfile" ] || [ -z "$destination" ]; then
    echo "Error: Missing mandatory options."
    echo "Usage: $0 --password [PASSWORD] --user [USER] --host [HOST] --localfile [LOCALFILE] --destination [DESTINATION] [--option [OPTION]] [--verbose] [--identity [FILE]] [--apt]"
    exit 1
fi

# Construct SSH options string
ssh_options_string=""
for opt in "${options[@]}"; do
    ssh_options_string+="-o $opt "
done

# Check if identity file is provided and is readable
if [ -n "$identity_file" ] && [ -r "$identity_file" ]; then
    # Identity file provided and readable, use scp with identity file
    ssh_command="scp1 $ssh_options_string $verbose_option -i $identity_file $localfile $user@$host:$destination"
else
    # No identity file, use sshpass
    ssh_command="sshpass -p $password scp1 $ssh_options_string $verbose_option $localfile $user@$host:$destination"
fi

echo "Executing command: $ssh_command"
eval $ssh_command

